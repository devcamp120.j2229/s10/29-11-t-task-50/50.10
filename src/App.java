public class App {
    public static void main(String[] args) throws Exception {
        /*
            In ra màn hình
            In ra terminal chuỗi
        */
        System.out.println("Hello, World!");
        System.out.println("Xin chào Devcamp");

        String appName = "Devcamp";
        // In ra màn hình chuỗi có giá trị của biến
        System.out.println("Hello, " + appName.length());
        System.out.println("Hello, " + appName.toUpperCase());
        System.out.println("Hello, " + appName.toLowerCase()); 
    }
}
